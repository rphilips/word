package word

import (
	"math/rand"
	"path/filepath"
)

// Word takes a (latin) word out of a predefined list of more than 2000.
// Words are always lowercase and contains only ASCII letters.
//
//	`minlen`: minimum length of the word
//	`maxlen`: maximum length of the word
//
// The software tries to find a word of minimal length.
// If no word is found, the software return the empty string.
//
// There are several options:
//
//	AtRandom: takes a random word
//	WordNotInSlice([]string): returns a word not in the given slice
//	WordNotInMap(map[string]any): returns a word not as key in a map
//	NoPrefixInDir(string): returns a word which is not used as a prefix to a file in a directory
//
// Examples:
//
//	Word(2, 5)
//	Word(2, 5, AtRandom)
//	Word(2, 5, NoPrefixInDir("/tmp"))
//	Word(2, 5, WordNotInSlice([]string{"aqua", "amor"}))

func Word(minlen int, maxlen int, options ...Option) string {
	config := NewCfg(options)
	words := make([]string, 0)
	for length := minlen; length <= maxlen; length++ {
		for _, w := range Latin {
			if len(w) != length {
				continue
			}
			if config.forbidden[w] {
				continue
			}
			if config.dir != "" {
				glob := filepath.Join(config.dir, w+"*")
				matches, _ := filepath.Glob(glob)
				if len(matches) != 0 {
					continue
				}
			}
			if !config.random {
				return w
			}
			words = append(words, w)
		}
	}

	if len(words) == 1 {
		return words[0]
	}
	if len(words) > 1 {
		k := rand.Intn(len(words))
		return words[k]
	}

	return ""
}

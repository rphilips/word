package word

import (
	"os"
	"testing"
)

func TestWord1(t *testing.T) {
	w := Word(2, 2)
	if w == "" {
		t.Errorf("%v", w)
	}

	w = Word(2, 2, AtRandom)
	if w == "" {
		t.Errorf("%v", w)
	}

	w = Word(1, 2)
	if len(w) < 2 {
		t.Errorf("%v", w)
	}
	w = Word(1, 2, AtRandom)
	if len(w) < 2 {
		t.Errorf("%v", w)
	}

	found := make([]string, 0)

	for i := 1; i < 9; i++ {
		w = Word(2, 2, WordNotInSlice(found))
		if w == "" {
			t.Errorf("%v", w)
		}
		found = append(found, w)
	}
	w = Word(2, 2, WordNotInSlice(found))
	if w != "" {
		t.Errorf("should be empty: `%v`", w)
	}
	w = Word(2, 3, WordNotInSlice(found))
	if len(w) != 3 {
		t.Errorf("should be length 3: `%v`", w)
	}

	w = Word(2, 3, WordNotInSlice(found), AtRandom)
	if len(w) != 3 {
		t.Errorf("should be length 3: `%v`", w)
	}
}

func TestWord2(t *testing.T) {
	dir, _ := os.MkdirTemp("", "word")
	defer os.RemoveAll(dir)
	w := Word(2, 2)
	v := Word(2, 2)

	if w != v {
		t.Errorf("words should be equal: `%v` == `%v`", w, v)
	}
	os.CreateTemp(dir, w)

	v = Word(2, 2, NoPrefixInDir(dir))
	if w == v {
		t.Errorf("words should NOT be equal: `%v` =/= `%v`", w, v)
	}
}

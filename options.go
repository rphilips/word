package word

type Config struct {
	dir       string
	forbidden map[string]bool
	random    bool
}

type Option func(*Config)

func NewCfg(opts []Option) Config {
	c := new(Config)
	for _, opt := range opts {
		opt(c)
	}

	return *c
}

func NoPrefixInDir(dir string) Option {
	return func(c *Config) {
		c.dir = dir
	}
}

func WordNotInSlice(values []string) Option {
	return func(c *Config) {
		c.forbidden = make(map[string]bool)
		for _, v := range values {
			c.forbidden[v] = true
		}
	}
}

func WordNotInMap(values map[string]any) Option {
	return func(c *Config) {
		if len(c.forbidden) == 0 {
			c.forbidden = make(map[string]bool)
			for v := range values {
				c.forbidden[v] = true
			}
		}
	}
}

func AtRandom(c *Config) {
	c.random = true
}
